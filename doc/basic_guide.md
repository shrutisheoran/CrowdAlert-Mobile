### Basic Dependencies:

This project is built using React Native. You will need the following dependencies.

#### 1. Node and Watchman

Install Node along with npm and Watchman.

**For MacOS:**

```
$ brew install node
$ brew install watchman
```

**For Linux:**

```
$ sudo apt-get install nodejs
$ sudo apt-get install npm
```

#### 2. React Native CLI

Install React Native CLI globally.

```
$ npm install -g react-native-cli
```

#### 3. NPM Dependencies

To install package-dependencies using NPM type this command in root directory of your project.

```
$ npm install
```
